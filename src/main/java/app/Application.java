package app;

import app.machine.drache.Drache;
import app.machine.drache.FeuerDrache;
import app.machine.fliegbar.Fliegbar;
import app.machine.drache.FressDrache;
import app.machine.flugzeug.Flugzeug;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 09.06.16.
 */
public class Application {

    public static void main(String[] args) {


//        Machine ferrariEngine = new ZweiTaktMotor();
//        Engine engine = null;
//        if (ferrariEngine instanceof Engine) {
//
//            engine = (Engine) ferrariEngine;
//            engine.refill (10,Engine.GASOLINE);
//        }



//        ZweiTaktMotor zweiTakter = new ZweiTaktMotor();
//        zweiTakter.refill(10, Engine.DIESEL);

        Drache drache = new FeuerDrache();
        drache.fliegeHerum();
        drache.spuckeFeuer();

        Flugzeug flugzeug = new Flugzeug();
        flugzeug.takeOff();



        List<Fliegbar> flugbareObjekte = new ArrayList<>();
        flugbareObjekte.add(drache);
        flugbareObjekte.add(flugzeug);


        for (Fliegbar fligbaresObjekt :flugbareObjekte) {
            fligbaresObjekt.fliegeHerum();
        }

    }


}
