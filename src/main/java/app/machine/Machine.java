package app.machine;

import app.machine.engine.Engine;

/**
 * Created by grazi on 09.06.16.
 */
public abstract class Machine {

    protected boolean isOn = false;

    public Machine () {
        super();
        System.out.println("Hallo zusammen, juhuu ich Machine bin da");
    }

    public void turnOn () {
        System.out.println("Turning machine on....");
        isOn = true;

    }

    public void turnOff () {
        System.out.println("Turning machine off....");
        isOn = false;
    }


}
